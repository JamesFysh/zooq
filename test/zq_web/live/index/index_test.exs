defmodule ZooQWeb.ZQLiveTest do
  import Plug.Conn
  import Phoenix.ConnTest
  import Phoenix.LiveViewTest

  use ZooQWeb.ConnCase, async: true

  test "disconnected content", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "You are not in the queue"
  end

  test "connected content", %{conn: conn} do
    conn = get(conn, "/")
    {:ok, _view, html} = live(conn)
    assert html =~ ~r/You are [0-9]+ \/ [0-9]+ in the queue/
    assert html =~ ~r/Your token is: [0-9a-f]{10}/
  end

  test "leave the queue", %{conn: conn} do
    conn = get(conn, "/")
    {:ok, view, html} = live(conn)
    assert html =~ ~r/You are [0-9]+ \/ [0-9]+ in the queue/
    assert html =~ ~r/Your token is: [0-9a-f]{10}/

    assert view
      |> element("button#queue_action")
      |> render_click() =~ "You are not in the queue"
  end

  test "rejoin the queue", %{conn: conn} do
    conn = get(conn, "/")
    {:ok, view, _html} = live(conn)
    view |> element("button#queue_action") |> render_click()

    assert view
      |> element("button#queue_action")
      |> render_click() =~ ~r/You are [0-9]+ \/ [0-9]+ in the queue/
  end
end
