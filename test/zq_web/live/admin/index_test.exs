defmodule ZooQWeb.ZQAdminLiveTest do
  import Plug.Conn
  import Phoenix.ConnTest
  import Phoenix.LiveViewTest

  use ZooQWeb.ConnCase, async: true

  test "disconnected content", %{conn: conn} do
    conn = get(conn, "/admin")
    assert html_response(conn, 200) =~ "Please enter administrative password to continue"
  end

  test "enter password", %{conn: conn} do
    conn = get(conn, "/admin")

    {:ok, view, html} = live(conn)
    assert html =~ "Please enter administrative password to continue"

    assert view
      |> element("#password_input")
      |> render_keyup(%{value: "12345"}) =~ "Admin</p>"
  end
end
