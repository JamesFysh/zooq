defmodule ZooQ.QueueTest do
  use ExUnit.Case, async: true

  test "initial state" do
    details = ZooQ.Queue.details(nil)

    assert details.position == nil
    assert details.token == nil
  end

  test "joining and leaving" do
    # Join the queue
    token = ZooQ.Queue.join()
    assert String.length(token) > 0

    # Validate details about our token within the queue
    details = ZooQ.Queue.details(token)
    assert details.count > 0
    assert details.position != nil
    assert details.position > 0
    assert details.in_queue == true
    assert details.admitted == false

    # Leave the queue, validate details update appropriately
    ZooQ.Queue.leave(token)
    details = ZooQ.Queue.details(token)
    assert details.position == nil
    assert details.in_queue == false
    assert details.admitted == false
  end
end
