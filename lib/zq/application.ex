defmodule ZooQ.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      ZooQWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: ZooQ.PubSub},
      # Start the Endpoint (http/https)
      ZooQWeb.Endpoint,
      # Start a worker by calling: ZooQ.Worker.start_link(arg)
      # {ZooQ.Worker, arg}
      ZooQ.Queue,
      ZooQ.PurchaseState,
      ZooQ.Throttle,
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ZooQ.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    ZooQWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
