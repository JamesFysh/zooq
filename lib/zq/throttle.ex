defmodule ZooQ.Throttle do
  use GenServer

  @moduledoc """
  Rather than broadcasting an update to all clients every time an event occurs,
  we use this module to throttle broadcasts to a maximum of one every 50ms.
  """
  def start_link(_state) do
    GenServer.start_link(__MODULE__, 0, name: __MODULE__)
  end

  @impl true
  def init(opts) do
    Process.send_after(self(), :tick, 50)
    {:ok, opts}
  end

  @impl true
  def handle_info(:tick, state) do
    ZooQ.Queue.broadcast()
    ZooQ.PurchaseState.broadcast()
    Process.send_after(self(), :tick, 50)
    {:noreply, state + 1}
  end
end
