defmodule ZooQ.PurchaseState do
  use Agent

  defmodule PState do
    defstruct [
      "1": 5,
      "2": 5,
      "3": 5,
      "4": 5,
      dirty: false,
    ]
  end

  def start_link(_initial) do
    Agent.start_link(fn -> %PState{} end, name: __MODULE__)
  end

  def details() do
    Agent.get(__MODULE__, & &1)
  end

  def reset() do
    Agent.update(__MODULE__, fn _ -> %PState{ dirty: true } end)
  end

  def purchase_one(day) do
    Agent.get_and_update(__MODULE__, &(update_state(day, &1)))
  end

  def broadcast do
    if Agent.get(__MODULE__, & &1.dirty) == true do
      Agent.update(__MODULE__, &(%{&1 | dirty: false}))
      Phoenix.PubSub.broadcast(ZooQ.PubSub, "state", {:updated})
    end
  end

  defp update_state(day, state) do
    current = Map.get(state, String.to_atom(day))
    case current do
      0 -> {false, state}
      _ -> {true, Map.put(state, String.to_atom(day), current - 1)}
    end
  end
end
