defmodule ZooQ.Queue do
  use Agent

  defmodule QState do
    defstruct [
      queue: [],
      admitted: [],
      token_total: 0,
      dirty: false,
      renders: 0,
    ]
  end

  def start_link(_initial) do
    Agent.start_link(fn -> %QState{} end, name: __MODULE__)
  end

  @doc """
  Called by a client to get updated details about the queue and their position within it
  """
  def details(token) do
    Agent.get(__MODULE__, & build_details(token, &1))
  end

  def admin_details do
    Agent.get(__MODULE__, fn state -> %{
      in_queue: length(state.queue),
      admitted: length(state.admitted),
      total_tokens: state.token_total,
      total_renders: state.renders,
    } end)
  end

  @doc """
  Called by the client-facing liveview each time render/1 fires to keep a tally of total renders executed
  """
  def on_render do
    Agent.update(__MODULE__, &(%{&1 | renders: &1.renders + 1}))
  end

  @doc """
  Called when a client will join the queue.  Returns a token that the client can then use to track their
  position in the queue.
  """
  def join do
    token = make_token()
    Agent.update(__MODULE__, &(%{&1 | queue: [token | &1.queue], token_total: &1.token_total + 1, dirty: true}))
    token
  end

  @doc """
  Called when a client requests to leave the queue.  The token is discarded.
  """
  def leave(token) do
    Agent.update(__MODULE__, &(%{&1 | queue: List.delete(&1.queue, token), dirty: true}))
  end

  @doc """
  Called when one token should be admitted from the queue.  Moves the token from the queue into the
  list of admitted tokens and returns the admitted token.
  """
  def admit_one() do
    l = Agent.get(__MODULE__, & &1.queue)
    if length(l) > 0 do
      {token, new_list} = List.pop_at(l, -1)
      Agent.update(__MODULE__, &(%{&1 | queue: new_list, admitted: [token | &1.admitted], dirty: true}))
      token
    end
  end

  @doc """
  Called when the queue state has changed, such as
  * a client joined the queue
  * a client left the queue
  * a token was admitted
  """
  def broadcast do
    if Agent.get(__MODULE__, & &1.dirty) == true do
      Agent.update(__MODULE__, &(%{&1 | dirty: false}))
      Phoenix.PubSub.broadcast(ZooQ.PubSub, "state", {:updated})
    end
  end

  defp make_token do
    for _ <- 1..10, into: "", do: <<Enum.random('0123456789abcdef')>>
  end

  defp build_details(token, state) do
    token_position = Enum.find_index(state.queue, &(&1 == token))
    position = case token_position do
      nil -> nil
      _ -> length(state.queue) - token_position
    end
    %{
      token: token,
      count: length(state.queue),
      position: position,
      in_queue: position != nil,
      admitted: Enum.find_index(state.admitted, &(&1 == token)) != nil
    }
  end
end
