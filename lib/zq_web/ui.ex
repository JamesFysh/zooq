defmodule ZooQWeb.UI do
  import Phoenix.LiveView.Helpers

  def container(assigns) do
    ~H"""
    <div class="border-4 rounded-xl pt-2 pb-3 sm:pb-5 md:pb-10 flex flex-col items-center mx-5 md:mx-20 lg:mx-40">
      <%= render_block(@inner_block) %>
    </div>
    """
  end

  def button(assigns) do
    ~H"""
    <button
        id={"#{@action}"}
        class={"border-2 border-blue-400 rounded-lg p-2 bg-blue-200 mb-4 w-#{@width}"}
        phx-click={"#{@action}"}
    >
        <%= @text %>
    </button>
    """
  end
end
