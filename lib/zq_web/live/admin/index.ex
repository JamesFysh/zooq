defmodule ZooQWeb.ZQAdminLive.Index do
  use ZooQWeb, :live_view
  alias ZooQWeb.UI

  @impl true
  def mount(_params, _session, socket) do
    if connected?(socket) do
      Phoenix.PubSub.subscribe(ZooQ.PubSub, "state")
    end
    socket = assign(socket, :authenticated, false)
    {:ok, assign(socket, ZooQ.Queue.admin_details())}
  end

  @impl true
  def render(assigns) do
    ZooQ.Queue.on_render()
    case assigns.authenticated do
      true ->
        ~H"""
        <UI.container>
          <p class="text-3xl">Admin</p>
          <p class="text-xl mt-4">Stats</p>
          <div><%= @in_queue %> users in the queue</div>
          <div><%= @admitted %> users admitted</div>
          <div><%= @total_tokens %> tokens created total</div>
          <div><%= @total_renders %> total renders</div>
          <p class="text-xl mt-4">Controls</p>
          <div>
            <UI.button text="Rejoin all clients" action="rejoin" width="40" />
            <UI.button text="Admit one" action="admit" width="40" />
            <UI.button text="Reset tickets" action="reset" width="40" />
          </div>
        </UI.container>
        """
      false ->
        ~H"""
        <UI.container>
          <p>
            Please enter administrative password to continue
          </p>
          <input class="border-2 border-blue-400 rounded-md" id="password_input" type="password" autofocus phx-keyup="password_updated" phx-throttle="100" />
        </UI.container>
        """
    end
  end

  @impl true
  def handle_event("rejoin", _params, socket) do
    Phoenix.PubSub.broadcast(ZooQ.PubSub, "state", {:rejoin})
    {:noreply, socket}
  end

  @impl true
  def handle_event("admit", _params, socket) do
    ZooQ.Queue.admit_one()
    {:noreply, socket}
  end

  @impl true
  def handle_event("reset", _params, socket) do
    ZooQ.PurchaseState.reset()
    {:noreply, socket}
  end

  @impl true
  def handle_event("password_updated", %{"value" => password}, socket) do
    {:noreply, assign(socket, %{authenticated: password == "12345"})}
  end

  @impl true
  def handle_info({:updated}, socket) do
    {:noreply, assign(socket, ZooQ.Queue.admin_details())}
  end

  @impl true
  def handle_info(_message, socket) do
    {:noreply, socket}
  end
end
