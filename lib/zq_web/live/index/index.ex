defmodule ZooQWeb.ZQLive.Index do
  use ZooQWeb, :live_view
  alias ZooQWeb.UI

  require Logger;

  @impl true
  def mount(_params, _session, socket) do
    socket = if connected?(socket) do
      token = ZooQ.Queue.join()
      Phoenix.PubSub.subscribe(ZooQ.PubSub, "state")
      refresh_state(socket, token)
    else
      assign(socket, %{token: nil, count: nil, position: nil, in_queue: false, admitted: false, available: %{}})
    end
    {:ok, socket}
  end

  @impl true
  def terminate(_reason, socket) do
    ZooQ.Queue.leave(socket.assigns.token)
  end

  @impl true
  def render(assigns) do
    ZooQ.Queue.on_render()
    ~H"""
    <UI.container>
      <p class="text-3xl mb-4 block min-w-full text-center">ZooQ</p>
      <p class="text-xl mb-4"><%= headline_text(assigns) %></p>
      <UI.button text={button_text(assigns)} action="queue_action" width="60" />
      <p class="text-sm mb-4">Your token is: <%= @token || "-" %></p>
      <div class="flex space-x-5 md:space-x-10 lg:space-x-20">
        <%= for day <- ~W(1 2 3 4) do %>
          <.live_component
            module={PurchaseButton}
            id={"day-#{day}"}
            day={day}
            admitted={assigns.admitted}
            available={available(day, assigns)}
          />
        <% end %>
      </div>
    </UI.container>
    """
  end

  @impl true
  def handle_event("queue_action", _params, socket) do
    if socket.assigns.in_queue do
      ZooQ.Queue.leave(socket.assigns.token)
      {:noreply, assign(socket, %{token: nil, in_queue: false})}
    else
      new_token = ZooQ.Queue.join()
      {:noreply, refresh_state(socket, new_token)}
    end
  end

  def handle_event("purchase_action", %{"day" => day}, socket) do
    ZooQ.PurchaseState.purchase_one(day)
    new_token = ZooQ.Queue.join()
    {:noreply, refresh_state(socket, new_token)}
  end

  @impl true
  def handle_info({:updated}, socket) do
    {:noreply, refresh_state(socket, socket.assigns.token)}
  end

  @impl true
  def handle_info({:rejoin}, socket) do
    case socket.assigns.in_queue do
      false ->
        new_token = ZooQ.Queue.join()
        {:noreply, refresh_state(socket, new_token)}
      _ ->
        {:noreply, socket}
    end
  end

  defp refresh_state(socket, token) do
    socket
    |> assign(ZooQ.Queue.details(token))
    |> assign(%{available: ZooQ.PurchaseState.details()})
  end

  defp headline_text(assigns) do
    case {assigns.in_queue, assigns.admitted} do
      {true, _} -> "You are #{assigns.position} / #{assigns.count} in the queue ⏲️"
      {false, true} -> "You have been admitted! 🎉"
      {false, false} -> "You are not in the queue 😢"
     end
  end

  defp button_text(assigns) do
    case {assigns.in_queue, assigns.admitted} do
      {true, _} -> "Leave the queue"
      {false, true} -> "Rejoin the queue"
      {false, false} -> "Join the queue"
    end
  end

  defp available(day, assigns) do
    Map.get(assigns.available, String.to_atom(day), "0")
  end
end
