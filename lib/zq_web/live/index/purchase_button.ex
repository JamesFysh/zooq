defmodule PurchaseButton do
  use Phoenix.LiveComponent

  def render(assigns) do
    {extra_classes, click_action} = case {assigns.admitted, assigns.available} do
      {false, _} -> {"border-gray-400 bg-gray-200 cursor-not-allowed", ""}
      {true, 0} -> {"border-red-400 bg-red-200 cursor-not-allowed", ""}
      {true, _} -> {"border-green-400 bg-green-200", "purchase_action"}
    end
    text = "Day #{assigns.day}"
    sub_text = "#{assigns.available} available"
    ~H"""
    <button
        class={"border-2 rounded-lg p-2 mb-4 w-30 disabled:opacity-50 #{extra_classes}"}
        phx-click={"#{click_action}"}
        phx-value-day={@day}
        disabled={assigns.admitted == false or assigns.available == "0"}
    >
        <span class="block text-sm"><%= text %></span>
        <span class="text-xs"><%= sub_text %></span>
    </button>
    """
  end
end
